﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{

    public void Scene_Loader(string Scene_Name)
    {
        SceneManager.LoadScene(Scene_Name);
    }
}

